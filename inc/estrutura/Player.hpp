#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <iostream>
#include <string>

using namespace std;

class Player{

    private:
        string name;
    public:

        Player();    
        ~Player();
        
        void set_name(string name);
        string get_name();

};


#endif