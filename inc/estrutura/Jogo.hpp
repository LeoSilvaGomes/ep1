#ifndef JOGO_HPP
#define JOGO_HPP

#include <iostream>
#include <string>

class Jogo{
    
    private:
        void modo();
        void single_player();
        void multi_player();
    public:

        Jogo();    
        ~Jogo();
        
        void menu();
        void como_jogar();

};


#endif