#ifndef RANK_HPP
#define RANK_HPP

#include <fstream>

using namespace std;

class Rank{
    public:

    Rank();
    ~Rank();

    void add_rank(fstream &arquivo, int pontuacao, string name);

    void imprimir();

};

#endif