#ifndef FERRAMENTA_HPP
#define FERRAMENTA_HPP

#include <iostream>
#include <string>

using namespace std;

template <typename IN>

IN getInput();
string getString();
void clear();
void sleep();
void clear_cin();
#endif