#ifndef MAPA_HPP
#define MAPA_HPP

#include <bits/stdc++.h>
#include <string>

using namespace std;

class Canoa;
class Submarino;
class PortaAvioes;
class Embarcacoes;

class Mapa{

    private:
    
        string nome;
        int tamanho_mapa;
        int cordx;
        int cordy;
        int atualiza_mapa;

    public:

        Mapa();
        ~Mapa();

        int get_tamanho_mapa();
        void set_tamanho_mapa(int tamanho_mapa);

        string get_nome();
        void set_nome(string nome);

        void set_atualiza_mapa_single(Mapa*, Mapa *mapa2, Canoa *canoa, Submarino *submarino, PortaAvioes *portaavioes, map<pair<int, int>, Embarcacoes*> &embarcacoes, int opcao);
        void set_atualiza_mapa_multi(Mapa*, Mapa *mapa2, Canoa *canoa, Submarino *submarino, PortaAvioes *portaavioes, map<pair<int, int>, Embarcacoes*> &embarcacoes, int derc, string nome_arquivo);

        int escolher_mapa();

        void criar_single(Canoa *canoa, PortaAvioes *portaavioes, Submarino *submarino, Mapa *mapa);
        void criar_multi(Canoa *canoa, PortaAvioes *portaavioes, Submarino *submarino, Mapa *mapa);

        void imprimir(int tamanho, std::map<std::pair<int, int>, Embarcacoes*> &local);
};

#endif