#ifndef PORTAAVIOES_HPP
#define PORTAAVIOES_HPP

#include <iostream>
#include "Embarcacoes.hpp"
#include "../mapa/Mapa.hpp"

class PortaAvioes: public Embarcacoes{

    private:
        int habilidade;
    public:
        PortaAvioes();
        ~PortaAvioes();

        int get_habilidade();
        void set_habilidade(int habilidade);

        int get_tamanho();
        void set_tamanho(int tamanho);

        void set_posicao(int tamanho_mapa, std::map<std::pair<int, int>, Embarcacoes*> &local, Mapa *map, fstream &arquivo);

        int get_direcao();
        void set_direcao(int direcao, int &tamanho_mapa, PortaAvioes *embarcacao, std::map<std::pair<int, int>, Embarcacoes*> &local);

};

#endif