#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include "../mapa/Mapa.hpp"
#include <bits/stdc++.h>

class Embarcacoes{

    private:

        int X;
        int Y;

    protected:
    
        int direcao;
        int tamanho;
        int habilidade;
    
    public:

        Embarcacoes();
        ~Embarcacoes();

        virtual int get_tamanho();
        virtual void set_tamanho(int tamanho);

        int get_direcao();
        void set_direcao(int direcao);

        int get_cordX();
        void set_cordX(int X);

        int get_cordY();
        void set_cordY(int Y);

        virtual void set_posicao(int tamanho_mapa, std::map<std::pair<int, int>, Embarcacoes*> &local, Mapa *map, fstream &arquivo) = 0;
        
        virtual int get_habilidade() = 0;
        virtual void set_habilidade(int habilidade) = 0;

};

#endif