#ifndef CANOA_HPP
#define CANOA_HPP

#include <iostream>
#include <string>
#include "Embarcacoes.hpp"
#include "../mapa/Mapa.hpp"

class Canoa: public Embarcacoes{

    public:

        Canoa();
        ~Canoa();

        int get_tamanho();
        void set_tamanho(int tamanho);

        void set_posicao(int tamanho_mapa, std::map<std::pair<int, int>, Embarcacoes*> &local, Mapa *map, fstream &arquivo);

        int get_habilidade();
        void set_habilidade(int habilidade);

};

#endif