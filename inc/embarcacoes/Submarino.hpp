#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include <iostream>
#include "Embarcacoes.hpp"
#include "../../inc/mapa/Mapa.hpp" 

class Submarino: public Embarcacoes{

    private:
        int habilidade;
    public:
        Submarino();
        ~Submarino();

        int get_habilidade();
        void set_habilidade(int habilidade);

        int get_tamanho();
        void set_tamanho(int tamanho);

        void set_posicao(int tamanho_mapa, std::map<std::pair<int, int>, Embarcacoes*> &local, Mapa *map, fstream &arquivo);

        int get_direcao();
        void set_direcao(int direcao, int &tamanho_mapa, Submarino *embarcacao, std::map<std::pair<int, int>, Embarcacoes*> &local);

};

#endif