# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Funcionalidades do projeto

* Esse trabalho foi programado na linguagem CPP, na apredizagem de alunos da Univerdade de Brasília - Faculdade Gama, terem um maior desenvolvimento sobre o conteúdo da matéria Orientação a Objetivos.
* Para este projeto foi ultilizado a editor Visual Studio Code.

# O Extermínio de Embarcações (OEE)

## Sinopse

![Capa OEE](/doc/imagem/Capa.png)

Um dia houve lugar para andar!
Um dia teve lugar para dirigir!
Mas isso já faz 86 anos e hoje os tempos são diferentes!
A guerra destruiu todos os lugares onde se tinha terra, com a força das bombas atômicas não há nada mais que o mar.
MAS A GUERRA AINDA NÃO ACABOU!!
Decida de qual lado vai ficar, porque não tem como ficar neutro!
Você comandará o esquadrão de mísseis para derrubar as embarcações inimigas, não perca tempo ou sua tripulação irá morrer!


### Menu

![Capa OEE](/doc/imagem/Menu.png)


### Mapa

![Capa OEE](/doc/imagem/Map.png)

## Instruções de como jogar

Para compilar:
```
$ make
```

Para rodar:
```
$ make run
```

## Dependências

Esse repositório usa a biblioteca Boost para fazer o split das palavras, para usa-la é necessário fazer a instalação

Podemos instalar essa biblioteca via apt-get ou yum: 
```
$ apt-get install libboost-dev -y
```
Ou:
```
$ yum install boost-devel -y 
```