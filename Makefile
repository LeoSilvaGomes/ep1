# Executavel
BINFOLDER := bin/
# .hpp
INCFOLDER := inc/
# .cpp
SRCFOLDER := src/
# .o
OBJFOLDER := obj/

CC := g++

CFLAGS := -W -Wall -ansi -std=c++14 -pedantic

SRCFILES := $(wildcard src/*.cpp)
SIDFILES := $(wildcard src/**/*.cpp) 

all: create $(SIDFILES:src/%.cpp=obj/%.o) $(SRCFILES:src/%.cpp=obj/%.o) 
	$(CC) $(CFLAGS) obj/**/*.o obj/*.o -o bin/prog

obj/%.o: src/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ -I./inc

run: bin/prog
	bin/prog

.PHONY: clean
clean:
	rm -rf obj/*
	rm -rf bin/*

create:
	if [ ! -d obj/embarcacoes ]; then mkdir obj/embarcacoes; fi
	if [ ! -d obj/mapa ]; then mkdir obj/mapa; fi
	if [ ! -d obj/estrutura ]; then mkdir obj/estrutura; fi
	if [ ! -d obj/ferramentas ]; then mkdir obj/ferramentas; fi