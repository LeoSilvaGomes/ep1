#include "../../inc/ferramentas/Ferramenta.hpp"
#include <bits/stdc++.h>
#include <unistd.h>
#include <string>

using namespace std;

template <typename IN>

IN getInput(){
    while(true){
        IN variavel;
        cin >> variavel;
        if(cin.fail()){
            clear_cin();
            cout << endl << "Entrada inválida. Insira novamente: ";            
        }
        else{
            clear_cin();
            return variavel;
        }
    }
}

string getString(){
    while(true){
        string variavel;
        try{
            getline(cin, variavel);
        }
        catch(const std::ios_base::failure& e){
            clear_cin();
            cout << "Entrada inválida - " << e.what() << " - Insira novamente: " << endl;
        }
        return variavel;

    }
}

void clear(){
    printf("\033[H\033[J");
}

void sleep(){
    usleep(2000000);
}

void clear_cin(){
    cin.clear();
    cin.ignore(32767,'\n');
}