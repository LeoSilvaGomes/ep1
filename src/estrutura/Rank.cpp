#include "../../inc/ferramentas/Ferramenta.hpp"
#include "../../inc/estrutura/Rank.hpp"
#include <boost/algorithm/string.hpp> 
#include <bits/stdc++.h>
#include <fstream>
#include <string>

using namespace std;

Rank::Rank(){}

Rank::~Rank(){}

void Rank::add_rank(fstream &arquivo, int pontuacao, string name){
    arquivo << name << " " << pontuacao << endl;
}

void Rank::imprimir(){
    vector<pair<string, int>> rank;
    vector<string> word_to_word;
    fstream rank1, rank2, rank3;
    string line;
    int i, f;

    rank1.open("doc/arquivo/rank1_multi", ios::in);
    rank2.open("doc/arquivo/rank2_multi", ios::in);
    rank3.open("doc/arquivo/rank_single", ios::in);

    clear();

    cout << "Rank Multi-Player:" << endl;

    if (rank1.is_open()){
        while (! rank1.eof()){
            getline(rank1, line);
            boost::split(word_to_word, line, boost::is_any_of(" "));
            rank.push_back(make_pair(word_to_word[0], stoi(word_to_word[1])));
        }
    }

    if (rank2.is_open()){
        while (! rank2.eof()){
            getline(rank2, line);
            boost::split(word_to_word, line, boost::is_any_of(" "));
            rank.push_back(make_pair(word_to_word[0], stoi(word_to_word[1])));
        }
    }

    sort(rank.begin(), rank.end());
    f = rank.size();

    for (i = 0; i < f; i++){
        cout << "#" << i << " " << rank[i].first << " " << rank[i].second << endl;
    }

    rank.clear();

    cout << endl << "Rank Single-Player:" << endl;

    if (rank3.is_open()){
        while (! rank3.eof()){
            getline(rank3, line);
            boost::split(word_to_word, line, boost::is_any_of(" "));
            rank.push_back(make_pair(word_to_word[0], stoi(word_to_word[1])));
        }
    }

    sort(rank.begin(), rank.end());
    f = rank.size();

    for (i = 0; i < f; i++){
        cout << "#" << i << " " << rank[i].first << " " << rank[i].second << endl;
    }

    cout << endl << endl << "Aperte ENTER para voltar ao menu";
    getchar();
}