#include "../../inc/ferramentas/Ferramenta.hpp"
#include "../../inc/embarcacoes/PortaAvioes.hpp"
#include "../../inc/embarcacoes/Submarino.hpp"
#include "../../inc/embarcacoes/Canoa.hpp"
#include "../../inc/estrutura/Player.hpp"
#include "../../inc/estrutura/Jogo.hpp"
#include "../../inc/estrutura/Rank.hpp"
#include <boost/algorithm/string.hpp> 
#include "../../inc/mapa/Mapa.hpp"
#include <bits/stdc++.h>
#include <iostream>
#include <unistd.h>
#include <string>

using namespace std;

Mapa *mapa = new Mapa();
Mapa *mapa2 = new Mapa();
Canoa *agua = new Canoa();
Canoa *canoa = new Canoa();
Rank *rank_sing = new Rank();
Player *player1 = new Player();
Player *player2 = new Player();
Rank *rank1_multi = new Rank();
Rank *rank2_multi = new Rank();
Rank *rank_historia = new Rank();
Submarino *submarino = new Submarino();
PortaAvioes *portaavioes = new PortaAvioes();
map<pair<int, int>, Embarcacoes*> embarcacoes_1;
map<pair<int, int>, Embarcacoes*> embarcacoes_2;


Jogo::Jogo(){}

Jogo::~Jogo(){

    clear();

    cout << "  Até logo e não esqueça que seu time está espernado por você  " << endl;
    cout << "                    ┏┓                     ┏┓" << endl;
    cout << "                    ┃┃  ┏━━┓   ┏━━┓ ┏━━┓   ┃┃" << endl;
    cout << "                    ┃┃  ┃┏┓┃   ┃┃━┫ ┃┃━┫   ┃┃" << endl;
    cout << "                    ┃┃  ┃┗┛┃   ┃┃━┫ ┃┃━┫   ┃┃" << endl;
    cout << "                    ┃┃  ┗━━┛   ┗━━┛ ┗━━┛   ┃┃" << endl;
    cout << "                    ┗┛                     ┗┛" << endl << endl;

}

void Jogo::menu(){

    clear();

    cout << "┏┓                     ┏┓" << endl;
    cout << "┃┃  ┏━━┓   ┏━━┓ ┏━━┓   ┃┃" << endl;
    cout << "┃┃  ┃┏┓┃   ┃┃━┫ ┃┃━┫   ┃┃" << endl;
    cout << "┃┃  ┃┗┛┃   ┃┃━┫ ┃┃━┫   ┃┃" << endl;
    cout << "┃┃  ┗━━┛   ┗━━┛ ┗━━┛   ┃┃" << endl;
    cout << "┗┛                     ┗┛" << endl << endl;
    cout << "   █▀▄▀█ █▀▀ █▀▀▄ █  █ " << endl;
    cout << "   █ ▀ █ █▀▀ █  █ █  █ " << endl;
    cout << "   ▀   ▀ ▀▀▀ ▀  ▀  ▀▀▀ " << endl;
    cout << "         1.Jogar                " << endl;
    cout << "         2.Mapas           " << endl;
    cout << "         3.Rank            " << endl;
    cout << "         4.Como Jogar      " << endl;
    cout << "         5.Sair            " << endl << endl << endl;
    cout << "Faça sua escolha" << endl;


    int opcao = getInput<int>();

    if (opcao == 1) Jogo::modo();
    else if (opcao == 2) {
        clear();
        cout << "O que você gostaria?" << endl << "1-Visualizar" << endl << "2-Cadastrar" << endl << endl << "Reposta: ";
        opcao = getInput<int>();
        clear();
        if(opcao == 1){
            cout << "Você gostaria de visualizar:" << endl << "1-Multiplayer" << endl << "2-SinglePlayer" << endl << endl << "Resposta: ";
            opcao = getInput<int>();
            if(opcao == 1){
                string line, nome_arquivo;
                vector<string> Menu;
                fstream arqu;

                mapa->set_tamanho_mapa(0);
                
                clear();

                arqu.open("doc/arquivo/mapa_multi/mapa_nome_multi", ios::in);
                getline(arqu, line);    
                boost::split(Menu, line, boost::is_any_of(" "));
                int i = Menu.size() - 1;

                cout << "Nome dos mapas: " << endl;
                while (i != -1){
                    cout << Menu[i] << endl;
                    i--;
                }
                cout << endl << endl;

                cout << "Qual o mapa do jogador?" << endl<< "Resposta: ";

                
                do{
                    i = Menu.size() - 1;
                    nome_arquivo.clear();
                    nome_arquivo = getString();
                    while (i != -1){
                        if (nome_arquivo == Menu[i]){
                            break;
                        }
                        i--;
                    }
                    if (nome_arquivo != Menu[i]){
                        cout << endl << "Entrada inválida. Insira novamente: "; 
                    }
                }while(nome_arquivo != Menu[i]); 
                
                arqu.close();
                
                clear();
                cout << "Mapa Player1" << endl << endl;
                cout << "Aperte ENTER para continuar!";
                getchar();
                mapa->set_atualiza_mapa_multi(mapa, mapa2, canoa, submarino, portaavioes, embarcacoes_1, 1, nome_arquivo);
                mapa->imprimir(mapa->get_tamanho_mapa(), embarcacoes_1);
                cout << "Aperte ENTER para continuar!";
                getchar();


                clear();
                cout << "Mapa Player2" << endl << endl;
                cout << "Aperte ENTER para continuar!";
                getchar();
                mapa2->set_atualiza_mapa_multi(mapa, mapa2, canoa, submarino, portaavioes, embarcacoes_2, 3, nome_arquivo);
                mapa2->imprimir(mapa2->get_tamanho_mapa(), embarcacoes_2);
                cout << "Aperte ENTER para continuar!";
                getchar();
                Jogo::menu();

            }
            else if (opcao == 2){

                clear();
                cout << "Mapa Player" << endl << endl;
                mapa->set_atualiza_mapa_single(mapa, mapa2, canoa, submarino, portaavioes, embarcacoes_1, 2);
                mapa->imprimir(mapa->get_tamanho_mapa(), embarcacoes_1);
                cout << "Aperte ENTER para continuar!";
                getchar();
                Jogo::menu();
            
            }
            else{
                clear();
                cout << "Escolha o número certo, minha senhora!" << endl;
                sleep();
                Jogo::menu();
            }            
        }
        else if (opcao == 2){
            clear();
            cout << "Você gostaria de cadastrar:" << endl << "1-Multiplayer" << endl << "2-SinglePlayer" << endl << endl << "Resposta: ";
            opcao = getInput<int>();
            if(opcao == 1){
                mapa->criar_multi(canoa, portaavioes, submarino, mapa);
            }
            else if (opcao == 2){
                mapa->criar_single(canoa, portaavioes, submarino, mapa);
            }
            else{
                clear();
                cout << "Escolha o número certo, minha senhora!" << endl;
                sleep();
                Jogo::menu();
            }
        }
        else{
            clear();
            cout << "Escolha o número certo, minha senhora!" << endl;
            sleep();
            Jogo::menu();
        }
    }
    else if (opcao == 3){
        rank_sing->imprimir();
        menu();
    }
    else if (opcao == 4){
        Jogo::como_jogar();
    }
    else if (opcao == 5){}
    else {
        clear();
        cout << "Escolha o número certo, minha senhora!" << endl;
        sleep();
        Jogo::menu();
    }

}

void Jogo::modo(){

    clear();

    cout << " ______________________________________ " << endl;
    cout << "|                                      | " << endl;
    cout << "|           1.Single-Player            |" << endl;
    cout << "|           2.Multi-Player             |" << endl;
    cout << "|           3.Voltar para o Menu       |" << endl;
    cout << "|______________________________________|" << endl << endl << endl;
    cout << "Faça sua escolha" << endl;


    int opcao = getInput<int>();

    if (opcao == 1) Jogo::single_player();
    else if (opcao == 2) Jogo::multi_player();
    else if (opcao == 3) Jogo::menu();
    else {
        clear();
        cout << "Escolha o número certo, minha senhora!" << endl;
        sleep(2000000);
        Jogo::modo();
    }

}

void Jogo::single_player(){

    map<pair<int, int>, Embarcacoes*> mapa_1;
    map<pair<int, int>, Embarcacoes*> mapa_2;
    int pontuacao1 = 10000, pontuacao2 = 10000;
    int del1 = 0, del2 = 0;
    fstream rank;
    int x, y;

    mapa->set_tamanho_mapa(0);

    clear();

    cout << "Qual o nome do jogador?" << endl << "Resposta: ";
    player1->set_name(getString());
    mapa->set_atualiza_mapa_single(mapa, mapa2, canoa, submarino, portaavioes, embarcacoes_1, 0);

    mapa2->set_atualiza_mapa_single(mapa, mapa2, canoa, submarino, portaavioes, embarcacoes_2, 1);

    clear();

    agua->set_habilidade(7);

    while(del1 != 15 && del2 != 15){
        cout << "Jogador " << player1->get_name() <<" atira:" << endl;
        mapa2->imprimir(mapa2->get_tamanho_mapa(), mapa_2);
        x = -1;
        y = -1;
        while (x > mapa2->get_tamanho_mapa() -1 || y > mapa2->get_tamanho_mapa() - 1 || y < 0 || x < 0){
            cout << "Escolha uma coordenada." << endl << "X: ";
            x = getInput<int>();
            cout << "Y: ";
            y = getInput<int>();
        }
        if (mapa_2[{x, y}] != NULL){
            clear();
            cout << "Para de ser sonsa, escolhe um lugar que você ainda não tentou" << endl << "Aperte ENTER para continuar";
            getchar();
        }
        else if (embarcacoes_2[{x, y}] != NULL){
            clear();
            if (embarcacoes_2[{x, y}]->get_tamanho() == 2){
                if (embarcacoes_2[{x, y}]->get_habilidade() == -1){
                    embarcacoes_2[{x, y}]->set_habilidade(1);
                }
                else{
                    embarcacoes_2[{x, y}]->set_habilidade(-1);
                }
            }
            else{
                embarcacoes_2[{x, y}]->set_habilidade(1);
            }

            if (embarcacoes_2[{x, y}]->get_habilidade() == 1){
                pontuacao1 = pontuacao1 + 50;
                mapa_2[{x, y}] = embarcacoes_2[{x, y}];
                del1++;
            }
            cout << endl << "Aperte ENTER para continuar";
            getchar();
        }
        else{
            pontuacao1 = pontuacao1 - 10;
            mapa_2[{x, y}] = agua;
            clear();
            cout << "Acertou o nada minha senhora!" << endl << "Aperte ENTER para continuar";
            getchar();
        }
        clear();



        mapa->imprimir(mapa->get_tamanho_mapa(), mapa_1);
        x = rand() %mapa->get_tamanho_mapa();
        y = rand() %mapa->get_tamanho_mapa();
        cout << endl << "Aperte ENTER para continuar";
        getchar();
        if (mapa_1[{x, y}] != NULL){
            clear();
            cout << "A maquina foi sonsa e clicou na mesma casa que clicou antes" << endl << "Aperte ENTER para continuar";
            getchar();
        }
        else if (embarcacoes_1[{x, y}] != NULL){
            clear();

            if (embarcacoes_1[{x, y}]->get_tamanho() == 2){
                if (embarcacoes_1[{x, y}]->get_habilidade() == -1){
                    embarcacoes_1[{x, y}]->set_habilidade(1);
                }
                else{
                    embarcacoes_1[{x, y}]->set_habilidade(-1);
                }
            }
            else{
                embarcacoes_1[{x, y}]->set_habilidade(1);
            }


            if (embarcacoes_1[{x, y}]->get_habilidade() == 1){
                pontuacao2 = pontuacao2 + 50;
                mapa_1[{x, y}] = embarcacoes_1[{x, y}];
                del2++;
            }
            cout << endl << "Aperte ENTER para continuar";
            getchar();
        }
        else{
            pontuacao2 = pontuacao2 - 10;
            mapa_1[{x, y}] = agua;
            clear();
            cout << "A maquina é burra, acertou nada!" << endl << "Aperte ENTER para continuar";
            getchar();
        }
        clear();
    }

    if (del1 == 15 || del2 == 15){
        cout << player1->get_name() << " GANHOU" << endl << endl;
        cout << "PONTUAÇÃO: " << endl << player1->get_name();
        printf(" %.8d\n", pontuacao1);
        cout << player1->get_name();
        printf(" %.8d\n", pontuacao2);
    }

    rank.open("doc/arquivo/rank_single", ios::app);
    rank_sing->add_rank(rank, pontuacao1, player1->get_name());
    rank.close();


    cout << endl << endl << "Aperte ENTER para voltar ao menu";
    getchar();
    menu();   

}

void Jogo::multi_player(){

    map<pair<int, int>, Embarcacoes*> mapa_1;
    map<pair<int, int>, Embarcacoes*> mapa_2;
    int pontuacao1 = 10000, pontuacao2 = 10000;
    fstream rank1, rank2, arqu;
    string line, nome_arquivo;
    int del1 = 0, del2 = 0;
    vector<string> Menu;
    int x, y, opcao;

    mapa->set_tamanho_mapa(0);

    clear();

    cout << "Você quer:" << endl << "1-Escolher mapas já criado" << endl << "2-Criar mapas" << endl << "Resposta: ";

    opcao = getInput<int>();
    if (opcao == 1){
        arqu.open("doc/arquivo/mapa_multi/mapa_nome_multi", ios::in);
        getline(arqu, line);    
        boost::split(Menu, line, boost::is_any_of(" "));
        int i = Menu.size() - 1;

        cout << "Nome dos mapas: " << endl;
        while (i != -1){
            cout << Menu[i] << endl;
            i--;
        }
        cout << endl << endl;

        cout << "Qual o mapa do jogador?" << endl<< "Resposta: ";

        
        do{
            i = Menu.size() - 1;
            nome_arquivo.clear();
            nome_arquivo = getString();
            while (i != -1){
                if (nome_arquivo == Menu[i]){
                    break;
                }
                i--;
            }
            if (nome_arquivo != Menu[i]){
                cout << endl << "Entrada inválida. Insira novamente: "; 
            }
        }while(nome_arquivo != Menu[i]); 
    }
    else if(opcao == 2){
        mapa->criar_single(canoa, portaavioes, submarino, mapa);
    }
    else{
        clear();
        cout << "Escolha o número certo, minha senhora!" << endl;
        sleep();
        multi_player();
    }

    clear();

    cout << "Qual o nome do primeiro jogar?" << endl << "Resposta: ";
    player1->set_name(getString());
    mapa->set_atualiza_mapa_multi(mapa, mapa2, canoa, submarino, portaavioes, embarcacoes_1, 1, nome_arquivo);
    
    clear();

    cout << "Qual o nome do segundo jogar?" << endl << "Resposta: ";
    player2->set_name(getString());
    mapa2->set_atualiza_mapa_multi(mapa, mapa2, canoa, submarino, portaavioes, embarcacoes_2, 3, nome_arquivo);

    
    clear();
    agua->set_habilidade(7);

    while(del1 != 15 && del2 != 15){
        cout << "Jogador " << player1->get_name() <<" atira:" << endl;
        mapa2->imprimir(mapa2->get_tamanho_mapa(), mapa_2);
        x = -1;
        y = -1;
        while (x > mapa2->get_tamanho_mapa() -1 || y > mapa2->get_tamanho_mapa() - 1 || y < 0 || x < 0){
            cout << "Escolha uma coordenada." << endl << "X: ";
            x = getInput<int>();
            cout << "Y: ";
            y = getInput<int>();
        }
        if (mapa_2[{x, y}] != NULL){
            clear();
            cout << "Para de ser sonsa, escolhe um lugar que você ainda não tentou" << endl << "Aperte ENTER para continuar";
            getchar();
        }
        else if (embarcacoes_2[{x, y}] != NULL){
            clear();
            if (embarcacoes_2[{x, y}]->get_tamanho() == 2){
                if (embarcacoes_2[{x, y}]->get_habilidade() == -1){
                    embarcacoes_2[{x, y}]->set_habilidade(1);
                }
                else{
                    embarcacoes_2[{x, y}]->set_habilidade(-1);
                }
            }
            else{
                embarcacoes_2[{x, y}]->set_habilidade(1);
            }

            if (embarcacoes_2[{x, y}]->get_habilidade() == 1){
                pontuacao1 = pontuacao1 + 50;
                mapa_2[{x, y}] = embarcacoes_2[{x, y}];
                del1++;
            }
            cout << endl << "Aperte ENTER para continuar";
            getchar();
        }
        else{
            pontuacao1 = pontuacao1 - 10;
            mapa_2[{x, y}] = agua;
            clear();
            cout << "Acertou o nada minha senhora!" << endl << "Aperte ENTER para continuar";
            getchar();
        }
        clear();



        cout << "Jogador " << player2->get_name() <<" atira:" << endl;
        mapa->imprimir(mapa->get_tamanho_mapa(), mapa_1);
        x = -1;
        y = -1;
        while (x > mapa->get_tamanho_mapa() -1 || y > mapa->get_tamanho_mapa() - 1 || y < 0 || x < 0){
            cout << "Escolha uma coordenada." << endl << "X: ";
            x = getInput<int>();
            cout << "Y: ";
            y = getInput<int>();
        }
        if (mapa_1[{x, y}] != NULL){
            clear();
            cout << "Para de ser sonsa, escolhe um lugar que você ainda não tentou" << endl << "Aperte ENTER para continuar";
            getchar();
        }
        else if (embarcacoes_1[{x, y}] != NULL){
            clear();

            if (embarcacoes_1[{x, y}]->get_tamanho() == 2){
                if (embarcacoes_1[{x, y}]->get_habilidade() == -1){
                    embarcacoes_1[{x, y}]->set_habilidade(1);
                }
                else{
                    embarcacoes_1[{x, y}]->set_habilidade(-1);
                }
            }
            else{
                embarcacoes_1[{x, y}]->set_habilidade(1);
            }


            if (embarcacoes_1[{x, y}]->get_habilidade() == 1){
                pontuacao2 = pontuacao2 + 50;
                mapa_1[{x, y}] = embarcacoes_1[{x, y}];
                del2++;
            }
            cout << endl << "Aperte ENTER para continuar";
            getchar();
        }
        else{
            pontuacao2 = pontuacao2 + 10;
            mapa_1[{x, y}] = agua;
            clear();
            cout << "Acertou o nada minha senhora!" << endl << "Aperte ENTER para continuar";
            getchar();
        }
        clear();
    }

    if (del2 == 15){
        cout << player2->get_name() << " GANHOU" << endl << endl;
        cout << "PONTUAÇÃO: " << endl << player1->get_name();
        printf(" %.8d\n", pontuacao1);
        cout << player2->get_name();
        printf(" %.8d\n", pontuacao2);
    }
    else{
        cout << player1->get_name() << "ganhou" << endl << endl;
        cout << "PONTUAÇÃO: " << endl << player1->get_name();
        printf(" %.8d\n", pontuacao1);
        cout << player2->get_name();
        printf(" %.8d\n", pontuacao2);
    }

    rank1.open("doc/arquivo/rank1_multi", ios::app);
    rank2.open("doc/arquivo/rank2_multi", ios::app);
    rank1_multi->add_rank(rank1, pontuacao1, player1->get_name());
    rank2_multi->add_rank(rank2, pontuacao2, player2->get_name());
    rank1.close();
    rank2.close();
    arqu.close();

    cout << endl << endl << "Aperte ENTER para voltar ao menu";
    getchar();
    menu();   
}

void Jogo::como_jogar(){

    clear();
    cout << "Um dia houve lugar para andar!" << endl << "Um dia teve lugar para dirigir!" << endl << "Mas isso já faz 86 anos e hoje os tempos são diferentes!" << endl << "A guerra destruiu todos os lugares onde se tinha terra, com a força das bombas atômicas não há nada mais que o mar." << endl << "MAS A GUERRA AINDA NÃO ACABOU!!" << endl << "Decida de qual lado vai ficar, porque não tem como ficar neutro!" << endl << "Você comandará o esquadrão de mísseis para derrubar as embarcações inimigas, não perca tempo ou sua tripulação irá morrer!";
    cout << "Aperte ENTER para continuar!";
    getchar();

    clear();
    cout << "O EXTERMINIO DE EMBARCAÇÕES" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();

    clear();
    cout << "Agora descobra quais são as funcionalidade nesse jogo" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();

    clear();
    cout << "Então no Menu pode ver 5 opções" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();

    clear();
    cout << "A primeira é o Jogar, onde você vai poder deestruir as embarcações do seu inimigo a vontade" << endl << "Mas antes você passará por duas etapas:" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();
 
    clear();
    cout << "Na primeira etapa você terá que escolher entre Multi-Player e Single-Player!" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();

    clear();
    cout << "Na segunda etapa você terá que cadastrar os usuários e escolher se você quer criar um mapa ou opinar por um já existente!" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();

    clear();
    cout << "Depois disso você pode destruir embarcações a vontade com seu amigo ou com a maquina!" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();

    clear();
    cout << "A segunda opção do menu é a opção Mapa, onde você vai pode visualizar ou cadastrar mapas da forma que quiser" << endl << "Lembre: que os mapas eles tem um tamanho de 13 à 20, então não fuja desses valores" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();

    clear();
    cout << "A Terceira opção do menu é a opção Rank, onde você irá poder visualizar as pontuações de todos os jogadores que já jogaram, separados por Single-Player e Multi-Player" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();
    
    clear();
    cout << "A Quarta opção do menu é a opção Como Jogar, onde você irá poder ter noção das possibilidades de opções dentro desse jogo" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();
    
    clear();
    cout << "A Quinta opção do menu é a opção Sair, onde você finaliza esse maravilhoso Jogo!" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();
    
    clear();
    cout << "Então é isso, vá a LUTA!" << endl;
    cout << "Aperte ENTER para continuar!";
    getchar();
    

    Jogo::menu();
}