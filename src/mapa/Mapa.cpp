#include "../../inc/ferramentas/Ferramenta.hpp"
#include "../../inc/embarcacoes/Embarcacoes.hpp"
#include "../../inc/embarcacoes/PortaAvioes.hpp"
#include "../../inc/embarcacoes/Submarino.hpp"
#include "../../inc/embarcacoes/Canoa.hpp"
#include "../../inc/estrutura/Jogo.hpp"
#include <boost/algorithm/string.hpp> 
#include "../../inc/mapa/Mapa.hpp"
#include <bits/stdc++.h>
#include <unistd.h>
#include <fstream>
#include <string>


using namespace std;
fstream arquivo;
fstream arquivo2;
Jogo *jogo = new Jogo;

map<pair<int, int>, Embarcacoes*> localizacao;


Mapa::Mapa() {
    cout << "Construdor da classe Mapa" << endl;
}

Mapa::~Mapa(){
    cout << "Destrudor da classe Mapa" << endl;
}

void Mapa::criar_single(Canoa *canoa, PortaAvioes *portaavioes, Submarino *submarino, Mapa *mapa){ 

    clear();
    cout << "Qual o nome do Mapa: ";
    set_nome(getString());
    arquivo.open("doc/arquivo/mapa_single/"+get_nome(), ios::out);   
    cout << endl;

    do{
        clear();
        cout << "Qual o tamanho do Mapa: ";
        set_tamanho_mapa(getInput<int>());
        if (get_tamanho_mapa() < 13 || get_tamanho_mapa() > 20){
            cout << "Meu Deus minha senhora, parece que não lê as coisas, na opção COMO JOGAR tem explicando certinho que:" << endl << "VOCÊ NÃO PODE COLOCAR MENOS DE 13 E MAIS DE 20 NO TAMANHO DO MAPA" << endl << "Parece que não pensa!";
            cout << endl << "Aperte ENTER para continuar:";
            getchar();
        }
    }while(get_tamanho_mapa() < 13 || get_tamanho_mapa() > 20);

    arquivo << "#Tamanho do Mapa\n" << get_tamanho_mapa() << " tamanho" << endl;
    cout << endl;

    clear(); 
    imprimir(get_tamanho_mapa(), localizacao);

    arquivo << "#Canoas\n";
    canoa->set_posicao(get_tamanho_mapa(), localizacao, mapa, arquivo);

    arquivo << "#Submarino\n";
    submarino->set_posicao(get_tamanho_mapa(), localizacao, mapa, arquivo);

    arquivo << "#Porta-Aviões\n";
    portaavioes->set_posicao(get_tamanho_mapa(), localizacao, mapa, arquivo);

    arquivo2.open("doc/arquivo/mapa_single/mapa_nome_single", ios::app);    
    arquivo2 << " " << get_nome();

    arquivo.close();
    arquivo2.close();

    clear();

    imprimir(get_tamanho_mapa(), localizacao);
    cout << "Mapa concluido!!";
    usleep(2000000);

    jogo->menu();
}

void Mapa::criar_multi(Canoa *canoa, PortaAvioes *portaavioes, Submarino *submarino, Mapa *mapa){ 

    int l = 1;

    clear();
    cout << "Qual o nome do Mapas: ";
    set_nome(getString());
    arquivo.open("doc/arquivo/mapa_multi/"+get_nome(), ios::out);
    cout << endl;

    do{
        clear();
        cout << "Qual o tamanho do Mapa: ";
        set_tamanho_mapa(getInput<int>());
        if (get_tamanho_mapa() < 13 || get_tamanho_mapa() > 20){
            cout << "Meu Deus minha senhora, parece que não lê as coisas, na opção COMO JOGAR tem explicando certinho que:" << endl << "VOCÊ NÃO PODE COLOCAR MENOS DE 13 E MAIS DE 20 NO TAMANHO DO MAPA" << endl << "Parece que não pensa!";
            cout << endl << "Aperte ENTER para continuar:";
            getchar();
        }
    }while(get_tamanho_mapa() < 13 || get_tamanho_mapa() > 20);

    arquivo << "#Tamanho do Mapa\n" << get_tamanho_mapa() << " tamanho" << endl;
    cout << endl;

    while (l != 3){
        arquivo << endl << endl << "# Player" << l << endl;

        localizacao.clear();

            getchar();
        clear(); 
        cout << "Player" << l << " escolha a posição das suas embarcações" << endl;
        sleep();
        clear();
        imprimir(get_tamanho_mapa(), localizacao);

        arquivo << "#Canoas\n";
        canoa->set_posicao(get_tamanho_mapa(), localizacao, mapa, arquivo);

        arquivo << "#Submarino\n";
        submarino->set_posicao(get_tamanho_mapa(), localizacao, mapa, arquivo);

        arquivo << "#Porta-Aviões\n";
        portaavioes->set_posicao(get_tamanho_mapa(), localizacao, mapa, arquivo);

        arquivo2.open("doc/arquivo/mapa_multi/mapa_nome_multi", ios::app);    
        arquivo2 << " " << get_nome();
        l++;
    }

    arquivo.close();
    arquivo2.close();

    clear();

    imprimir(get_tamanho_mapa(), localizacao);
    cout << "Mapa concluido!!";
    usleep(2000000);

    jogo->menu();
}

void Mapa::imprimir(int tamanho, std::map<std::pair<int, int>, Embarcacoes*> &local){ // Método imprimir mapa
    
    for(int i = -1; i <= tamanho; i++){
        for(int l = -1; l <= tamanho; l++){
            if (i == -1){
                if(l == -1)cout << "┏   ";
                else if(l == tamanho)cout << "┓";
                else {
                    if(l > 9)cout << l << " ";
                    else{cout << " " << l << " ";}
                }
            }
            else if(i == tamanho){
                if(l == -1)cout << "┗━━━";
                else if(l == tamanho)cout << "┛";
                else cout << "━━━";                
            }
            else if(l == -1){
                if(i > 9)cout << "┃" << i << " ";
                else{cout << "┃ " << i << " ";}
            }
            else if(l == tamanho) cout << "┃";
            else{
                if(local[{i, l}] == NULL)cout << " ~ ";
                else if(local[{i, l}]->get_habilidade() == 7) cout << " o ";
                else{cout << " x " ;}
            }    
         }
        cout << endl;
    }

}



//Get(s) da class Mapa

string Mapa::get_nome(){
    return nome;
}

int Mapa::get_tamanho_mapa(){
    return tamanho_mapa;
}


//Set(s) da class Mapa

void Mapa::set_nome(string nome){

    this->nome = nome; 

}

void Mapa::set_tamanho_mapa(int tamanho_mapa){

    this->tamanho_mapa = tamanho_mapa;
}

void Mapa::set_atualiza_mapa_single(Mapa *mapa, Mapa *mapa2, Canoa *canoa, Submarino *submarino, PortaAvioes *portaavioes, map<pair<int, int>, Embarcacoes*> &embarcacoes, int single){
    
    vector<string> word_to_word;
    string line, nome_arquivo;
    fstream arqui2, arqu;
    vector<string> Menu;
    int tamanho, opcao;
    
    Menu.clear();
    word_to_word.clear();

    clear();


    if (single == 0){
        cout << "Você quer:" << endl << "1-Escolher mapa já criado" << endl << "2-Criar mapa" << endl << "Resposta: ";
        opcao = getInput<int>();
        if (opcao == 1){
            arqu.open("doc/arquivo/mapa_single/mapa_nome_single", ios::in);
            getline(arqu, line);    
            boost::split(Menu, line, boost::is_any_of(" "));
            int i = Menu.size() - 1;

            cout << "Nome dos mapas: " << endl;
            while (i != -1){
                cout << Menu[i] << endl;
                i--;
            }
            cout << endl << endl;

            cout << "Qual o mapa do jogador?" << endl<< "Resposta: ";

            
            do{
                i = Menu.size() - 1;
                nome_arquivo.clear();
                nome_arquivo = getString();
                while (i != -1){
                    if (nome_arquivo == Menu[i]){
                        break;
                    }
                    i--;
                }
                if (nome_arquivo != Menu[i]){
                    cout << endl << "Entrada inválida. Insira novamente: "; 
                }
            }while(nome_arquivo != Menu[i]);

            arqui2.open("doc/arquivo/mapa_single/"+nome_arquivo, ios::in);
        }
        else if(opcao == 2){
            getchar();
            criar_single(canoa, portaavioes, submarino, mapa);
        }
        else{
            clear();
            cout << "Escolha o número certo, minha senhora!" << endl;
            sleep();
            set_atualiza_mapa_single(mapa, mapa2, canoa, submarino, portaavioes, embarcacoes, single);
        }
    }
    else if (single == 2){
        arqu.open("doc/arquivo/mapa_single/mapa_nome_single", ios::in);
        getline(arqu, line);    
        boost::split(Menu, line, boost::is_any_of(" "));
        int i = Menu.size() - 1;

        cout << "Nome dos mapas: " << endl;
        while (i != -1){
            cout << Menu[i] << endl;
            i--;
        }
        cout << endl << endl;

        cout << "Qual o mapa do jogador?" << endl<< "Resposta: ";

        
        do{
            i = Menu.size() - 1;
            nome_arquivo.clear();
            nome_arquivo = getString();
            while (i != -1){
                if (nome_arquivo == Menu[i]){
                    break;
                }
                i--;
            }
            if (nome_arquivo != Menu[i]){
                cout << endl << "Entrada inválida. Insira novamente: "; 
            }
        }while(nome_arquivo != Menu[i]);

        arqui2.open("doc/arquivo/mapa_single/"+nome_arquivo, ios::in);
    }
    else{
        arqui2.open("doc/arquivo/mapa_single/single", ios::in);
    }

    if (arqui2.is_open()){
        while (! arqui2.eof()){
            getline(arqui2, line);
            boost::split(word_to_word, line, boost::is_any_of(" "));
            if (word_to_word.size() > 1 && word_to_word[1] == "tamanho"){
                if (mapa->get_tamanho_mapa() == 0){
                    mapa->set_tamanho_mapa(stoi(word_to_word[0]));
                    tamanho = mapa->get_tamanho_mapa(); 
                }
                else{
                    mapa2->set_tamanho_mapa(stoi(word_to_word[0]));
                    tamanho = mapa2->get_tamanho_mapa(); 
                }
            }
            else if (word_to_word.size() > 3 && word_to_word[2] == "canoa"){
                embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}] = canoa;
            }
            else if (word_to_word.size() > 3 && word_to_word[2] == "submarino"){
                embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}] = submarino;
                embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}]->set_cordX(stoi(word_to_word[0]));
                submarino->set_cordY(stoi(word_to_word[1]));
                submarino->set_direcao(stoi(word_to_word[3]), tamanho, submarino, embarcacoes);
            }
            else if (word_to_word.size() > 3 && word_to_word[2] == "portaavioes"){
                embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}] = portaavioes;
                portaavioes->set_cordX(stoi(word_to_word[0]));
                portaavioes->set_cordY(stoi(word_to_word[1]));
                portaavioes->set_direcao(stoi(word_to_word[3]), tamanho, portaavioes, embarcacoes);
            }
        }
        arqui2.close();
    }
    else cout << "Unable to open file"; 

    arqu.close();

}

void Mapa::set_atualiza_mapa_multi(Mapa *mapa, Mapa *mapa2, Canoa *canoa, Submarino *submarino, PortaAvioes *portaavioes, map<pair<int, int>, Embarcacoes*> &embarcacoes, int derc, string nome_arquivo){
    
    vector<string> word_to_word;
    fstream arqui2;
    string line;
    int tamanho;


    clear();

    arqui2.open("doc/arquivo/mapa_multi/"+nome_arquivo, ios::in);

    if (arqui2.is_open()){
        while (! arqui2.eof()){
            word_to_word.clear();
            getline(arqui2, line);
            boost::split(word_to_word, line, boost::is_any_of(" "));
            if (word_to_word.size() > 1 && word_to_word[1] == "tamanho"){
                mapa->set_tamanho_mapa(stoi(word_to_word[0]));
                tamanho = mapa->get_tamanho_mapa(); 
                mapa2->set_tamanho_mapa(stoi(word_to_word[0]));
                tamanho = mapa->get_tamanho_mapa(); 
            }
            if (word_to_word.size() > 1 && word_to_word[1] == "Player1"  && derc == 1){
                derc++;
            }
            else if (word_to_word.size() > 1 && word_to_word[1] == "Player2" && derc == 2){
                break;
            }
            else if (derc == 2){
                if (word_to_word.size() > 3 && word_to_word[2] == "canoa"){
                    embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}] = canoa;
                }
                else if (word_to_word.size() > 3 && word_to_word[2] == "submarino"){
                    embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}] = submarino;
                    embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}]->set_cordX(stoi(word_to_word[0]));
                    submarino->set_cordY(stoi(word_to_word[1]));
                    submarino->set_direcao(stoi(word_to_word[3]), tamanho, submarino, embarcacoes);
                }
                else if (word_to_word.size() > 3 && word_to_word[2] == "portaavioes"){
                    embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}] = portaavioes;
                    portaavioes->set_cordX(stoi(word_to_word[0]));
                    portaavioes->set_cordY(stoi(word_to_word[1]));
                    portaavioes->set_direcao(stoi(word_to_word[3]), tamanho, portaavioes, embarcacoes);
                }
            }
            if (word_to_word.size() > 1 && word_to_word[1] == "Player2" && derc == 3){
                derc++;
            }
            else if (derc == 4){
                if (word_to_word.size() > 3 && word_to_word[2] == "canoa"){
                    embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}] = canoa;
                }
                else if (word_to_word.size() > 3 && word_to_word[2] == "submarino"){
                    embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}] = submarino;
                    embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}]->set_cordX(stoi(word_to_word[0]));
                    submarino->set_cordY(stoi(word_to_word[1]));
                    submarino->set_direcao(stoi(word_to_word[3]), tamanho, submarino, embarcacoes);
                }
                else if (word_to_word.size() > 3 && word_to_word[2] == "portaavioes"){
                    embarcacoes[{stoi(word_to_word[0]), stoi(word_to_word[1])}] = portaavioes;
                    portaavioes->set_cordX(stoi(word_to_word[0]));
                    portaavioes->set_cordY(stoi(word_to_word[1]));
                    portaavioes->set_direcao(stoi(word_to_word[3]), tamanho, portaavioes, embarcacoes);
                }
            }
            else{
                continue;
            }
        }
        arqui2.close();
    }
    else cout << "Unable to open file"; 

}
