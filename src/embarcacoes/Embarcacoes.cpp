#include "../../inc/embarcacoes/Embarcacoes.hpp"
#include <iostream>

using namespace std;

Embarcacoes::Embarcacoes(){}
Embarcacoes::~Embarcacoes(){}

int Embarcacoes::get_tamanho(){
    return tamanho;
}
void Embarcacoes::set_tamanho(int tamanho){
    this->tamanho = tamanho;
}

int Embarcacoes::get_direcao(){
    return direcao;
}
void Embarcacoes::set_direcao(int direcao){
    this->direcao = direcao;
}

int Embarcacoes::get_cordY(){
    return Y;
}
void Embarcacoes::set_cordY(int Y){
    this->Y = Y;
}

int Embarcacoes::get_cordX(){
    return X;
}
void Embarcacoes::set_cordX(int X){
    this->X = X;
}

int Embarcacoes::get_habilidade(){
    return habilidade;
}
void Embarcacoes::set_habilidade(int habilidade){
    this->habilidade = habilidade;
}
