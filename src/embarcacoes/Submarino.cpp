#include "../../inc/embarcacoes/Submarino.hpp"
#include "../../inc/ferramentas/Ferramenta.hpp"
#include <unistd.h>
#include <iostream>

using namespace std;

Submarino::Submarino(){
    set_tamanho(2);
    cout << "Construdor da classe Submarino" << endl;
}

Submarino::~Submarino(){
    cout << "Destrudor da classe Submarino" << endl;
}


void Submarino::set_posicao(int tamanho_mapa, std::map<std::pair<int, int>, Embarcacoes*> &localizacao, Mapa *mapa, fstream &arquivo){
    int loop;
    Submarino *submarino = new Submarino();
    loop = 1;
    while (loop != 5){
        cout << "Me fale a coordenada da " << loop << "º Submarino" << endl << "X: ";
        set_cordX(getInput<int>());
        cout << "Y: ";
        set_cordY(getInput<int>());
        cout << endl;
        if(localizacao[{get_cordX(), get_cordY()}] == NULL && get_cordY() < tamanho_mapa && get_cordX() < tamanho_mapa
        && get_cordY() >= 0 && get_cordX() >= 0){
            cout << "Me fale para qual lado você quer posicionar a sua embarcação!" << endl;
            cout << "1.Cima" << endl << "2.Baixo" << endl << "3.Esquerda" << endl << "4.Direita" << endl << "Res:";
            set_direcao(getInput<int>(), tamanho_mapa, submarino, localizacao);
            if(get_direcao() != -1){
                localizacao[{get_cordX(), get_cordY()}] = submarino;
                arquivo << get_cordX() << " " << get_cordY() << " " << "submarino " << get_direcao() << endl;
            }
            else{
                clear();
                cout << "Escolha o número certo, minha senhora!" << endl;
                usleep(2000000);
                clear();
                mapa->imprimir(tamanho_mapa, localizacao);            
                continue;
            }
        }
        else{
            clear();
            cout << "Escolha o número certo, minha senhora!" << endl;
            usleep(2000000);
            clear();
            mapa->imprimir(tamanho_mapa, localizacao);            
            continue;
        }
        loop++;
        clear();
        mapa->imprimir(tamanho_mapa, localizacao);
    }
}

void Submarino::set_direcao(int direcao, int &tamanho_mapa, Submarino *embarcacao, std::map<std::pair<int, int>, Embarcacoes*> &localizacao){
    int loop;
    loop = 1;
    if(direcao == 1){
        while (loop != Submarino::get_tamanho()){
            if(localizacao[{get_cordX()-loop, get_cordY()}] == NULL && get_cordY() < tamanho_mapa && get_cordX()-loop < tamanho_mapa
            && get_cordY() >= 0 && get_cordX()-loop >= 0){
                loop++; 
                continue;
            }
            else{
                direcao = -1;
                break;
            }
        }
        if(direcao != -1){
            localizacao[{get_cordX()-1, get_cordY()}] = embarcacao;
        }
    }
    else if (direcao == 2){
        while (loop != Submarino::get_tamanho()){
            if(localizacao[{get_cordX()+loop, get_cordY()}] == NULL && get_cordY() < tamanho_mapa && get_cordX()+loop < tamanho_mapa
            && get_cordY() >= 0 && get_cordX()+loop >= 0){
                loop++;
                continue;
            }
            else{
                direcao = -1;
                break;
            }
        }
        if(direcao != -1){
            localizacao[{get_cordX()+1, get_cordY()}] = embarcacao;
        }        
    }
    else if (direcao == 3){
        while (loop != Submarino::get_tamanho()){
            if(localizacao[{get_cordX(), get_cordY()-loop}] == NULL && get_cordY()-loop < tamanho_mapa && get_cordX() < tamanho_mapa
            && get_cordY()-loop >= 0 && get_cordX() >= 0){
                loop++;
                continue;
            }
            else{
                direcao = -1;
                break;
            }
        }
        if(direcao != -1){
            localizacao[{get_cordX(), get_cordY()-1}] = embarcacao;
        }        
    }
    else if (direcao == 4){
        while (loop != Submarino::get_tamanho()){
            if(localizacao[{get_cordX(), get_cordY()+loop}] == NULL && get_cordY()+loop < tamanho_mapa && get_cordX() < tamanho_mapa
            && get_cordY()+loop >= 0 && get_cordX() >= 0){
                loop++;
                continue;
            }
            else{
                direcao = -1;
                break;
            }
        }
        if(direcao != -1){
            localizacao[{get_cordX(), get_cordY()+1}] = embarcacao;
        }  
    }
    else{
        direcao = -1;
    }
    this->direcao = direcao;
}

int Submarino::get_direcao(){
    return direcao;
}


int Submarino::get_tamanho(){
    return tamanho;
}
void Submarino::set_tamanho(int tamanho){
    this->tamanho = tamanho;
}


void Submarino::set_habilidade(int habilidade){
    if (habilidade == -1){
        cout << "Você acabou de acertar um Submarino, acerte-o mais uma vez para destrui-lo";
    }
    else{
        cout << "Você acabou de destruir um Submarino";
    }
    this->habilidade = habilidade;
}

int Submarino::get_habilidade(){
    return habilidade;
}