#include "../../inc/embarcacoes/PortaAvioes.hpp"
#include "../../inc/ferramentas/Ferramenta.hpp"
//#include "../../inc/mapa/Mapa.hpp"
#include <unistd.h>
#include <iostream>

using namespace std;


PortaAvioes::PortaAvioes(){
    set_tamanho(4);
    cout << "Construdor da classe PortaAvioes" << endl;
}

PortaAvioes::~PortaAvioes(){
    cout << "Destrudor da classe PortaAvioes" << endl;
}



void PortaAvioes::set_posicao(int tamanho_mapa, std::map<std::pair<int, int>, Embarcacoes*> &localizacao, Mapa *mapa, fstream &arquivo){
    int loop;
    PortaAvioes *portaavioes = new PortaAvioes();
    loop = 1;
    while (loop != 3){
        cout << "Me fale a coordenada do Porta-Aviões" << endl << "X: ";
        set_cordX(getInput<int>());
        cout << "Y: ";
        set_cordY(getInput<int>());
        cout << endl;
        if(localizacao[{get_cordX(), get_cordY()}] == NULL && get_cordY() < tamanho_mapa && get_cordX() < tamanho_mapa
        && get_cordY() >= 0 && get_cordX() >= 0){
            cout << "Me fale para qual lado você quer posicionar a sua embarcação!" << endl;
            cout << "1.Cima" << endl << "2.Baixo" << endl << "3.Esquerda" << endl << "4.Direita" << endl << "Res:";
            set_direcao(getInput<int>(), tamanho_mapa, portaavioes, localizacao);
            if(get_direcao() != -1){
                localizacao[{get_cordX(), get_cordY()}] = portaavioes;
                arquivo << get_cordX() << " " << get_cordY() << " " << "portaavioes " << get_direcao() << endl;
            }
            else{
                clear();
                cout << "Escolha o número certo, minha senhora!" << endl;
                usleep(2000000);
                clear();
                mapa->imprimir(tamanho_mapa, localizacao);            
                continue;
            }
        }
        else{
            clear();
            cout << "Escolha o número certo, minha senhora!" << endl;
            usleep(2000000);
            clear();
            mapa->imprimir(tamanho_mapa, localizacao);            
            continue;
        }
        loop++;
        clear();
        mapa->imprimir(tamanho_mapa, localizacao);
    }
}

void PortaAvioes::set_direcao(int direcao, int &tamanho_mapa, PortaAvioes *embarcacao, std::map<std::pair<int, int>, Embarcacoes*> &localizacao){
    int loop;
    loop = 1;
    if(direcao == 1){
        while (loop != PortaAvioes::get_tamanho()-1){
            if(localizacao[{get_cordX()-loop, get_cordY()}] == NULL && get_cordY() < tamanho_mapa && get_cordX()-loop < tamanho_mapa
            && get_cordY() >= 0 && get_cordX()-loop >= 0){
                loop++; 
                continue;
            }
            else{
                direcao = -1;
                break;
            }
        }
        if(direcao != -1){
            localizacao[{get_cordX()-1, get_cordY()}] = embarcacao;
            localizacao[{get_cordX()-2, get_cordY()}] = embarcacao;
            localizacao[{get_cordX()-3, get_cordY()}] = embarcacao;
        }
    }
    else if (direcao == 2){
        while (loop != PortaAvioes::get_tamanho()-1){
            if(localizacao[{get_cordX()+loop, get_cordY()}] == NULL && get_cordY() < tamanho_mapa && get_cordX()+loop < tamanho_mapa
            && get_cordY() >= 0 && get_cordX()+loop >= 0){
                loop++;
                continue;
            }
            else{
                direcao = -1;
                break;
            }
        }
        if(direcao != -1){
            localizacao[{get_cordX()+1, get_cordY()}] = embarcacao;
            localizacao[{get_cordX()+2, get_cordY()}] = embarcacao;
            localizacao[{get_cordX()+3, get_cordY()}] = embarcacao;
        }        
    }
    else if (direcao == 3){
        while (loop != PortaAvioes::get_tamanho()-1){
            if(localizacao[{get_cordX(), get_cordY()-loop}] == NULL && get_cordY()-loop < tamanho_mapa && get_cordX() < tamanho_mapa
            && get_cordY()-loop >= 0 && get_cordX() >= 0){
                loop++;
                continue;
            }
            else{
                direcao = -1;
                break;
            }
        }
        if(direcao != -1){
            localizacao[{get_cordX(), get_cordY()-1}] = embarcacao;
            localizacao[{get_cordX(), get_cordY()-2}] = embarcacao;
            localizacao[{get_cordX(), get_cordY()-3}] = embarcacao;
        }        
    }
    else if (direcao == 4){
        while (loop != PortaAvioes::get_tamanho()-1){
            if(localizacao[{get_cordX(), get_cordY()+loop}] == NULL && get_cordY()+loop < tamanho_mapa && get_cordX() < tamanho_mapa
            && get_cordY()+loop >= 0 && get_cordX() >= 0){
                loop++;
                continue;
            }
            else{
                direcao = -1;
                break;
            }
        }
        if(direcao != -1){
            localizacao[{get_cordX(), get_cordY()+1}] = embarcacao;
            localizacao[{get_cordX(), get_cordY()+2}] = embarcacao;
            localizacao[{get_cordX(), get_cordY()+3}] = embarcacao;
        }  
    }
    else{
        direcao = -1;
    }
    this->direcao = direcao;
}

int PortaAvioes::get_direcao(){
    return direcao;
}


int PortaAvioes::get_tamanho(){
    return tamanho;
}

void PortaAvioes::set_tamanho(int tamanho){
    this->tamanho = tamanho;
}

void PortaAvioes::set_habilidade(int habilidade){
    habilidade = rand() % 2;
    if (habilidade == 1){
        cout << "Você acabou de acertar um Porta-Aviões!" << endl;
    }
    if (habilidade == 0){
        cout << "Você tentou acertar um Porta-Aviões, mas ele desviou!" << endl;
    }
    this->habilidade = habilidade;
}

int PortaAvioes::get_habilidade(){
    return habilidade;
}