#include "../../inc/embarcacoes/Canoa.hpp"
#include "../../inc/ferramentas/Ferramenta.hpp"
#include "../../inc/mapa/Mapa.hpp"
#include <unistd.h>
#include <iostream>

using namespace std;

template <typename IN>

IN getInput(){
    while(true){
        IN variavel;
        cin >> variavel;
        if(cin.fail()){
            clear_cin();
            cout << endl << "Erro na entrada. Você pode digita-la novamente: ";
        }
        else{
            clear_cin(); 
            return variavel;
        }
    }
}

Canoa::Canoa() {
    set_tamanho(1);
    cout << "Construdor da classe Canoa" << endl;
}
Canoa::~Canoa(){
    cout << "Destrudor da classe Canoa" << endl;
}

void Canoa::set_posicao(int tamanho_mapa, std::map<std::pair<int, int>, Embarcacoes*> &localizacao, Mapa *mapa, fstream &arquivo){
    int loop;
    Canoa *canoa = new Canoa();
    loop = 1;
    while (loop != 7){
        cout << "Me fale a coordenada da " << loop << "º Canoa" << endl << "X: ";
        set_cordX(getInput<int>());
        cout << "Y: ";
        set_cordY(getInput<int>());
        cout << endl;
        if(localizacao[{get_cordX(), get_cordY()}] == NULL && get_cordY() < tamanho_mapa && get_cordX() < tamanho_mapa
        && get_cordY() >= 0 && get_cordX() >= 0){
            localizacao[{get_cordX(), get_cordY()}] = canoa;
            arquivo << get_cordX() << " " << get_cordY() << " " << "canoa" << " x" << endl;
        }
        else{
            clear();
            cout << "Escolha o número certo, minha senhora!" << endl;
            usleep(2000000);
            clear();
            mapa->imprimir(tamanho_mapa, localizacao);            
            continue;
        }
        loop++;
        clear();
        mapa->imprimir(tamanho_mapa, localizacao);
    }
}

int Canoa::get_habilidade(){
    return habilidade;
}

void Canoa::set_habilidade(int habilidade){
    if(habilidade == 1){
        cout << "Você acabou de acertar uma Canoa !";
    }
    this->habilidade = habilidade;
}

int Canoa::get_tamanho(){
    return tamanho;
}

void Canoa::set_tamanho(int tamanho){
    this->tamanho = tamanho;
}